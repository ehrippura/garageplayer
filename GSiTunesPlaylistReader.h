//
//  iTunesPlaylistReader.h
//  iTunesPlaylist
//
//  Created by Lin on 2006/8/11.
//  Copyright 2006 Ehrippura Seruziu Lin. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface GSiTunesPlaylistReader : NSObject {
	NSDictionary * m_iTunesData;
}

- (NSArray *)playlists;
- (NSArray *)musicUrlForPlaylist:(NSString *)listName;
- (NSArray *)tracksForPlaylist:(NSString *)listName;


@end
