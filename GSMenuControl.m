#import "GSMenuControl.h"
#import "GSPlayListElement.h"

@implementation GSMenuControl

- (id)init
{
	if (self = [super init]) {
		m_nowPlaying = [[NSSound alloc] init];
		m_playingPath = [[NSMutableString alloc] init];
		m_byDoubleClick = NO;
		
		return self;
	}
	
	return nil;
}

- (void)awakeFromNib
{
//	[m_playList setDoubleAction:@selector(GSDoubleClickAction:)];
	[m_playingField setStringValue:NSLocalizedString(@"Playlist is Empty", nil)];
}

- (void)dealloc
{
	[m_musicFiles release];
	[m_playingPath release];
	[super dealloc];
}

// NSApplication Delegate
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
	return YES;
}

// 從對話框中取得檔案
- (IBAction)addNewFiles:(id)sender
{
	NSArray * supportType = [self getSupportMusicType];
	NSOpenPanel * openPanel = [NSOpenPanel openPanel];

	SEL sel = @selector(openPanelDidEnd:returnCode:);
	
	// 打開 Open Panel
	[openPanel setAllowsMultipleSelection:YES];
//	int result = [openPanel runModalForDirectory:[NSHomeDirectory() stringByAppendingPathComponent:@"Music"]
//							   file:@""
//							  types:supportType];
	[openPanel beginSheetForDirectory:@"~/Music" 
                                 file:nil 
                                types:supportType 
                       modalForWindow:[m_playList window]	// 取得目前使用的視窗
                        modalDelegate:self
                       didEndSelector:sel
                          contextInfo:nil];

//	if (result == NSOKButton)
//		[self setPlayListView:[openPanel filenames]];

	[supportType release];
}

- (void)openPanelDidEnd:(NSOpenPanel *)sheet 
             returnCode:(int)returnCode 
{ 
    if (returnCode == NSOKButton)
		[self setPlayListView:[sheet filenames]];
} 

//===========================================================================
// Music Play Back
// 播放選擇中的音樂
- (IBAction)play:(id)sender
{
	int selectSound = [m_playList selectedRow];
	
	if ([m_nowPlaying isPlaying]) {
		[self stop:nil];
		[m_nowPlaying release];
	}
	
	m_byDoubleClick = NO;
	
	if (selectSound != -1) {
		[m_playingPath setString:[[m_musicFiles objectAtIndex:selectSound] path]];
		
		m_nowPlaying = [[NSSound alloc] initWithContentsOfFile:m_playingPath
												   byReference:YES];
		
		[m_nowPlaying setDelegate:self];
		
		[m_nowPlaying play];
		[m_playButton setHidden:YES];
		[m_stopButton setHidden:NO];
		[m_isPlayingMark startAnimation:nil];
		[m_playingField setStringValue:NSLocalizedString(@"Playing...", nil)];
	}
}

- (void)sound:(NSSound *)sound didFinishPlaying:(BOOL)aBool
{
	NSLog(@"Playback Finished");
	
	[m_playButton setHidden:NO];
	[m_stopButton setHidden:YES];
	[m_isPlayingMark stopAnimation:nil];
	[m_playingField setStringValue:NSLocalizedString(@"Select a music file", nil)];

	if (!m_byDoubleClick) {
		
		if ([m_playingPath isEqualToString:
			[[m_musicFiles objectAtIndex:[m_playList selectedRow]] path]]) {
		
			int nowRow = [m_playList selectedRow] + 1;
		
			if (nowRow < [m_musicFiles count]) {
				[m_playList selectRow:nowRow byExtendingSelection:NO];
				[m_playList scrollRowToVisible:nowRow];
			}
			else
				return;
		}
		[self play:nil];
	}
}

// 停止正在播放中的音樂
- (IBAction)stop:(id)sender
{	
	if (sender != nil)
		m_byDoubleClick = YES;
	
	if ([m_nowPlaying isPlaying]) {
		[m_nowPlaying stop];
	}
}

//
//==================================================================================


// 設定音樂檔案資料
- (void)setPlayListView:(NSArray *)filePaths
{
	int i;
	NSMutableArray * musicFiles = [NSMutableArray arrayWithCapacity:[filePaths count]];	

	for (i = 0; i < [filePaths count]; i++) {
		GSPlayListElement * element = [[GSPlayListElement alloc]
			initWithStrings:[self getSoundName:[filePaths objectAtIndex:i]]
				  withPath:[filePaths objectAtIndex:i]
				  withType:[[filePaths objectAtIndex:i] pathExtension]];
		
		[musicFiles addObject:element];
	}

	[self setMusicFiles:musicFiles];
}

- (NSString *)getSoundName:(NSString *)filePath
{
	NSFileManager * fm = [NSFileManager defaultManager];
	
	if ([fm fileExistsAtPath:filePath])
		return [filePath substringFromIndex:[[filePath stringByDeletingLastPathComponent] length] + 1];
	
	return nil;
}

// 回傳支援的檔案類型
- (NSArray *)getSupportMusicType
{
	NSMutableArray * supportType = [[NSMutableArray alloc] init];
	
	[supportType addObject:@"mp3"];
	[supportType addObject:@"mp4"];
	[supportType addObject:@"m4a"];
	[supportType addObject:@"wav"];
	[supportType addObject:@"mid"];
	[supportType addObject:@"mov"];
	
//	return [supportType retain];
	return supportType;
}

- (NSArray *)musicFiles
{
	return m_musicFiles;
}

- (void)setMusicFiles:(NSArray *)musicFiles
{
	if (m_musicFiles != musicFiles) {
		[m_musicFiles release];
		m_musicFiles = [musicFiles retain];
	}
	
	[m_playList reloadData];
	[m_playingField setStringValue:NSLocalizedString(@"Select a music file", nil)];
}

//=========================================================================
// Setting Table View

- (int)numberOfRowsInTableView:(NSTableView *)aTableView
{
	return [m_musicFiles count];
}

- (id)tableView:(NSTableView *)aTableView
objectValueForTableColumn:(NSTableColumn *)aTableColumn
			row:(int)rowIndex
{
	NSString * identifier = [aTableColumn identifier];

	if (aTableView == m_playList) {
		if ([identifier isEqualToString:@"sound"])
			return [[m_musicFiles objectAtIndex:rowIndex] sound];
		else if ([identifier isEqualToString:@"path"])
			return [[m_musicFiles objectAtIndex:rowIndex] path];
		else
			return [[m_musicFiles objectAtIndex:rowIndex] type];
	}
/*
	if (aTableView == m_playList)
		[[m_musicFiles objectAtIndex:rowIndex] objectForKey:identifier];
*/	
	return nil;
}

- (void)GSDoubleClickAction
{
	[m_isPlayingMark stopAnimation:nil];
	m_byDoubleClick = YES;
	
	NSLog(@"A Double Clock Happened");
	[self play:nil];
}

//
//=========================================================================

@end
