//
//  iTunesPlaylistReader.m
//  iTunesPlaylist
//
//  Created by Lin on 2006/8/11.
//  Copyright 2006 Ehrippura Seruziu Lin. All rights reserved.
//

#import "GSiTunesPlaylistReader.h"


@implementation GSiTunesPlaylistReader

- (id)init
{
	// 初始化將 iTunes Library xml 的檔案讀取出來
	if (self = [super init]) {
		NSString * home = NSHomeDirectory();
		NSString * iTunesXML = [home stringByAppendingPathComponent:@"Music/iTunes/iTunes Music Library.xml"];
		m_iTunesData = [[NSDictionary alloc] initWithContentsOfFile:iTunesXML];
	}
	
	return self;
}

- (void)dealloc
{
	[m_iTunesData release];
	[super dealloc];
}

- (NSArray *)playlists
{
	NSArray * playlistArray = [m_iTunesData objectForKey:@"Playlists"];
	NSMutableArray * playlistNames = [NSMutableArray array];
	
	int i, numberOfPlaylists = [playlistArray count];
	
	for (i = 0; i < numberOfPlaylists; i++) {
		NSDictionary * playlistEntry = [playlistArray objectAtIndex:i];
		NSString * playlistName = [playlistEntry objectForKey:@"Name"];
		
		[playlistNames addObject:playlistName];
	}
	
	return playlistNames;
}

- (NSArray *)tracksForPlaylist:(NSString *)listName
{
	NSArray * playlistArray = [m_iTunesData objectForKey:@"Playlists"];
	NSMutableArray * trackID = [NSMutableArray array];
	
	int i;
	for (i = 0; i < [playlistArray count]; i++) {
		NSDictionary * playlistEntry = [playlistArray objectAtIndex:i];
		
		if ([[playlistEntry objectForKey:@"Name"] isEqualToString:listName]) {
			NSArray * playlistItems = [playlistEntry objectForKey:@"Playlist Items"];
			
			NSEnumerator * enumerator = [playlistItems objectEnumerator];
			id obj = nil;
			
			while (obj = [enumerator nextObject]) 
				[trackID addObject:[obj objectForKey:@"Track ID"]];
		}
	}
	
	return trackID;
}

- (NSArray *)musicUrlForPlaylist:(NSString *)listName;
{
	NSArray * trackNumbers = [self tracksForPlaylist:listName];
	NSMutableArray * files = [NSMutableArray array];
	NSDictionary * allTracks = [m_iTunesData objectForKey:@"Tracks"];
	
	NSEnumerator * enumerator = [trackNumbers objectEnumerator];
	
	id obj = nil;
	
	while (obj = [enumerator nextObject]) {
		NSDictionary * track = [allTracks objectForKey:[obj stringValue]];
		NSURL * fileurl = [NSURL URLWithString:[track objectForKey:@"Location"]];
		[files addObject:[fileurl path]];
	}
	
	return files;
}

@end


