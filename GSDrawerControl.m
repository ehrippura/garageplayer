#import "GSDrawerControl.h"
#import "GSMenuControl.h"

@implementation GSDrawerControl

//============================================================================
// Drawer initialization

- (id)init
{
	if (self = [super init]) {
		m_reader = [[GSiTunesPlaylistReader alloc] init];
		return self;
	}
	
	return nil;
}

- (void)dealloc
{
	[m_reader dealloc];
	[super dealloc];
}

//
//=============================================================================

- (IBAction)addToPlaylist:(id)sender
{
	NSString * selectedPlaylistName = [[m_reader playlists] objectAtIndex:
		[m_iTunesPlaylistView selectedRow]];
	
	NSMutableArray * filepaths = [NSMutableArray array];
	NSArray * fileURL = [m_reader musicUrlForPlaylist:selectedPlaylistName];
	
	int i;
	for (i = 0; i < [fileURL count]; i++) {
		NSString * path = [fileURL objectAtIndex:i];
		[filepaths addObject:path];
	}
	
	[m_menuControl setPlayListView:filepaths];
	[m_drawer close];
}

//=========================================================================
// Setting Table View

- (int)numberOfRowsInTableView:(NSTableView *)aTableView
{
	return [[m_reader playlists] count];
}

- (id)tableView:(NSTableView *)aTableView
objectValueForTableColumn:(NSTableColumn *)aTableColumn
			row:(int)rowIndex
{
	NSString * identifier = [aTableColumn identifier];
	
	if ([identifier isEqualToString:@"number"])
		return [NSNumber numberWithInt:rowIndex + 1];
	else
		return [[m_reader playlists] objectAtIndex:rowIndex];
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
	NSString * selectedPlaylistName = [[m_reader playlists] objectAtIndex:
		[m_iTunesPlaylistView selectedRow]];
	
	[m_playlistMessage setStringValue:selectedPlaylistName];
}

- (void)viewDoubleClickAction
{
//	NSLog(@"iTunes playlist double click");
	[self addToPlaylist:nil];
}

//
//========================================================================

@end
