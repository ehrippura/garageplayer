/* GSMenuControl */

#import <Cocoa/Cocoa.h>

@interface GSMenuControl : NSObject
{
    IBOutlet id m_isPlayingMark;
    IBOutlet id m_playButton;
    IBOutlet id m_playingField;
    IBOutlet id m_playList;
	IBOutlet id m_stopButton;
	
	NSArray * m_musicFiles;
	NSSound * m_nowPlaying;
	NSMutableString * m_playingPath;
	BOOL m_byDoubleClick;
}
- (IBAction)addNewFiles:(id)sender;
- (IBAction)play:(id)sender;
- (IBAction)stop:(id)sender;

- (NSArray *)getSupportMusicType;

- (NSArray *)musicFiles;
- (void)setMusicFiles:(NSArray *)musicFiles;

- (void)setPlayListView:(NSArray *)filePaths;

- (NSString *)getSoundName:(NSString *)filePath;

@end
