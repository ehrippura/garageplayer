/* GSDrawerControl */

#import <Cocoa/Cocoa.h>

#import "GSiTunesPlaylistReader.h"

@interface GSDrawerControl : NSObject
{
    IBOutlet NSDrawer *m_drawer;
    IBOutlet id m_iTunesPlaylistView;
    IBOutlet id m_menuControl;
    IBOutlet id m_playlistMessage;
	
	GSiTunesPlaylistReader * m_reader;
}
- (IBAction)addToPlaylist:(id)sender;

@end
