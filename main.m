//
//  main.m
//  GaragePlayer
//
//  Created by Lin on 2006/8/5.
//  Copyright Ehrippura Seruziu Lin 2006. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
