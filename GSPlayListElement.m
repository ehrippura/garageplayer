//
//  GSPlayListElement.m
//  GaragePlayer
//
//  Created by Lin on 2006/8/6.
//  Copyright 2006 Ehrippura Seruziu Lin. All rights reserved.
//

#import "GSPlayListElement.h"


@implementation GSPlayListElement

	// initialize
- (id)initWithStrings:(NSString *)sound
			 withPath:(NSString *)path
			 withType:(NSString *)type
{
	if (self = [super init]) {
		m_sound = [sound copy];
		m_path = [path copy];
		m_type = [type copy];
	}
	
	return self;
}

- (void)dealloc
{
	[m_sound release];
	[m_path release];
	[m_type release];
	
	[super dealloc];
}

- (NSString *)sound
{
	return m_sound;
}

- (NSString *)path
{
	return m_path;
}

- (NSString *)type
{
	return m_type;
}

@end
