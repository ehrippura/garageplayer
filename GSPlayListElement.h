//
//  GSPlayListElement.h
//  GaragePlayer
//
//  Created by Lin on 2006/8/6.
//  Copyright 2006 Ehrippura Seruziu Lin. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface GSPlayListElement : NSObject {
	NSString * m_sound;
	NSString * m_path;
	NSString * m_type;
}

- (id)initWithStrings:(NSString *)sound
			  withPath:(NSString *)path
			  withType:(NSString *)type;

- (NSString *)sound;
- (NSString *)path;
- (NSString *)type;

@end
